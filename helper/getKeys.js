'use strict'

/**
  Given a arbitrary JSON, return all the keys whoes value is "JOHN"
  {
    "a": {
      "e": [{"f": "JOHN", "g": "DOE"}]
    },
    "b": {
      "h": "JANE",
    },
    "c": {
      "i": {
        "k": "DOE"
        "l": {
          "m": {
            "n": "JOHN",
            "o": "DOE"
          }
        }
      },
      "j": "JOHN"
    },
    "d": "JOHN",
    "e": "DOE"
  }
*/

const getKeys = (json, value, result) => {
  if (typeof result === 'undefined') {
    result = []
  }
  for (let key in json) {
    if (json[key] === value) {
      result.push(key)
    } else if (json[key] !== null && typeof json[key] === 'object') {
      getKeys(json[key], value, result)
    }
  }
  return result
}

module.exports = getKeys
