'use strict'

const fetch = require('node-fetch')

const USER_QUERY = '{users{id, firstName, lastName}}'
const REQUEST_PARAMETER = {
  url: 'http://localhost:8080/graphql',
  method: 'POST',
  headers: {'Content-Type': 'application/graphql'}
}

const graphqlRequest = async (body) => {
  try {
    return fetch(REQUEST_PARAMETER.url, {
      method: REQUEST_PARAMETER.method,
      headers: REQUEST_PARAMETER.headers,
      body: body
    })
  } catch (e) {
    console.log(e)
  }
}

const getUsersCount = async () => {
  try {
    let res = await graphqlRequest(USER_QUERY)
    let json = await res.json()
    return json.data.users.length
  } catch (e) {
    console.log(e)
  }
}

module.exports = getUsersCount
