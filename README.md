# NOTES
- All required tasks have been finished, and there are some tests to validate the logic. To run the tests: `npm test`

# FCG Code Challenge - test1 for micro-services

Currently this service is only able to return all our users.

### We would need your help with the following tasks:

 * A - extend the service to be able to get a single user based on the userid
 * B - rewrite the getUsersCount helper (helper/getUsersCount) to use async / await
 * C - write an helper function (helper/getKeys.js) that, given an arbitrary JSON object, returns all the keys whose value is matching one received as an input.
 * D - having a new permission system, to add / remove permissions ('cardealer', 'carinspector') to a user

Please add those feature to the current graphql schema (see the schema directory)

Bonus:
If you really want to stand out from the crowd please write tests for the resolvers/helpers (using any testing framework you like).

## Getting started
  - Node 8
  - npm install
  - npm run migrate (running the database migrations)
  - npm start (starting the service)

## Notes and Hints
 - You might not be able to finish everything: in case you got stuck with a task just proceed with the following one
 - Please use SQLite as a database and Sequelize as the ORM
 - Feel free to use GraphiQL api interface, for your convenience it is hosted under http://localhost:${port}/graphql
 - The default query for getting the users is {users{id, firstname, lastname}}
 - You might end up writing a new migration for the new permission system
 - There is no need to create resolvers for managing new permissions, like creating/deleting new permissions through the api
