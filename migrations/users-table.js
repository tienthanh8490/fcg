'use strict'

const path = require('path')
const user = require(path.resolve('model', 'user'))
const permission = require(path.resolve('model', 'permission'))

const TABLE_NAME = 'users'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      TABLE_NAME,
      {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },
        firstName: {type: Sequelize.STRING, allowNull: false},
        lastName: {type: Sequelize.STRING, allowNull: false},
        createdAt: {
          type: Sequelize.DATE,
          defaultValue: () => Date.now(),
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          defaultValue: () => Date.now(),
          allowNull: false
        },
        deletedAt: {type: Sequelize.DATE}
      },
      {freezeTableName: true, timestamps: true, paranoid: true}
    )

    await queryInterface.createTable(
      'permissions',
      {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },
        userId: {
          type: Sequelize.UUID,
          onDelete: 'CASCADE',
          references: {
            model: 'users',
            key: 'id'
          }
        },
        permission: {type: Sequelize.STRING, allowNull: false},
        createdAt: {
          type: Sequelize.DATE,
          defaultValue: () => Date.now(),
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          defaultValue: () => Date.now(),
          allowNull: false
        },
        deletedAt: {type: Sequelize.DATE}
      },
      {freezeTableName: true, timestamps: true, paranoid: true}
    )

    let carInspector = await user.create({firstName: 'Jane', lastName: 'Doe'})
    let carDealer = await user.create({firstName: 'John', lastName: 'Doe'})
    let carDealerPermission = await permission.create({id: 1, permission: 'cardealer'})
    let carInspectorPermission = await permission.create({id: 2, permission: 'carinspector'})
    carInspectorPermission.setUser(carInspector)
    carDealerPermission.setUser(carDealer)
  },

  down: (queryInterface, Sequelize) => {
    /* ignore */
  }
}
