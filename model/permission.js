'use strict'

const path = require('path')
const orm = require(path.resolve('orm'))
const Sequelize = require('sequelize')
const User = require(path.resolve('model', 'user'))

const Permission = orm.define(
  'permission',
  {
    permission: Sequelize.STRING
  }
)

Permission.belongsTo(User)

module.exports = Permission
