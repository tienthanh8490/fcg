'use strict'

const path = require('path')
const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLID,
  GraphQLNonNull
} = require('graphql')
const user = require(path.resolve('model', 'user'))

const userType = new GraphQLObjectType({
  description: 'single user',
  name: 'user',
  fields: {
    id: {type: GraphQLID},
    firstName: {type: GraphQLString},
    lastName: {type: GraphQLString}
  }
})

const schema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
      /**
       * user query, returns a list of our users
       */
      users: {
        type: new GraphQLList(userType),
        resolve () {
          return user.findAll({raw: true})
        }
      },
      user: {
        type: userType,
        args: {
          id: {
            description: 'query: get user by id',
            type: GraphQLNonNull(GraphQLString)
          }
        },
        resolve (obj, args) {
          return user.find({
            where: {
              id: args.id
            },
            raw: true
          })
        }
      }
    }
  })
})

module.exports = schema
