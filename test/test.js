const expect = require('chai').expect
const getKeys = require('../helper/getKeys')
const app = require('../index')
const request = require('supertest')
const userId = '855f6e68-8817-4acf-8ce9-c929adf0b806'

describe('Helper', () => {
  describe('#getKeys()', () => {
    it('should work correctly for string input', () => {
      const obj = 'abc'
      expect(getKeys(obj, 'JOHN')).to.deep.equal([])
    })
    it('should work correctly for empty input', () => {
      const obj = {}
      expect(getKeys(obj, 'JOHN')).to.deep.equal([])
    })
    it('should work correctly for null input', () => {
      const obj = null
      expect(getKeys(obj, 'JOHN')).to.deep.equal([])
    })
    it('should work correctly for array input', () => {
      const obj = ['THIS', 'IS', 'A', 'TEST', 'JOHN', 'DOE']
      expect(getKeys(obj, 'JOHN')).to.deep.equal(['4'])
    })
    it('should work correctly for input of mixed types', () => {
      const obj = {
        'a': {
          'e': [{'f': 'JOHN', 'g': 'DOE'}]
        },
        'b': {
          'h': 'JANE'
        },
        'c': {
          'i': {
            'k': 'DOE',
            'l': {
              'm': {
                'n': 'JOHN',
                'o': 'DOE'
              }
            }
          },
          'j': 'JOHN'
        },
        'd': 'JOHN',
        'e': null
      }
      expect(getKeys(obj, 'JOHN')).to.deep.equal(['f', 'n', 'j', 'd'])
    })
  })
})

describe('GraphQL', () => {
  it('Should be able to return all users', (done) => {
    request(app).post('/graphql')
      .send({ query: '{users{id, firstName, lastName}}'})
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.body.data.users.length).to.be.equal(2)
        expect(res.body.data.users[0]).to.have.property('id')
        expect(res.body.data.users[0]).to.have.property('firstName')
        expect(res.body.data.users[0]).to.have.property('lastName')
        done()
      })
  })
  it('Should be able to retrieve user using id', (done) => {
    request(app).post('/graphql')
      .send({ query: `{user(id:"${userId}"){id, firstName, lastName}}`})
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.body.data.user.firstName).to.be.equal('Jane')
        done()
      })
  })
})
